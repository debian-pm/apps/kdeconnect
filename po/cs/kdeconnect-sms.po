# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
# Vit Pelcak <vit@pelcak.org>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-02-04 03:16+0100\n"
"PO-Revision-Date: 2020-01-17 16:29+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 19.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: conversationlistmodel.cpp:185 conversationmodel.cpp:135
#, kde-format
msgid "(Unsupported Message Type)"
msgstr "(Nepodporovaný typ zprávy)"

#: conversationlistmodel.cpp:189
#, kde-format
msgid "You: %1"
msgstr "Vy: %1"

#: conversationlistmodel.cpp:195
#, kde-format
msgid "%1: %2"
msgstr "%1: %2"

#: main.cpp:39
#, kde-format
msgid "SMS Instant Messaging"
msgstr "Chat přes SMS"

#: main.cpp:41 qml/main.qml:35
#, kde-format
msgid "KDE Connect SMS"
msgstr "KDE Connect SMS"

#: main.cpp:43
#, kde-format
msgid "(C) 2018-2019, KDE Connect Team"
msgstr "(C) 2018-2019, Tým KDE Connect"

#: main.cpp:44
#, kde-format
msgid "Simon Redman"
msgstr "Simon Redman"

#: main.cpp:45
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:46
#, kde-format
msgid "Nicolas Fella"
msgstr "Nicolas Fella"

#: main.cpp:54
#, kde-format
msgid "Select a device"
msgstr "Zvolte zařízení"

#: main.cpp:54
#, kde-format
msgid "id"
msgstr "id"

#: main.cpp:55
#, kde-format
msgid "Send a message"
msgstr "Poslat správu"

#: main.cpp:55
#, kde-format
msgid "message"
msgstr "zpráva"

#: qml/ChatMessage.qml:122
#, kde-format
msgid "Copy Message"
msgstr "Kopírovat zprávu"

#: qml/ChatMessage.qml:150
#, kde-format
msgid "Spoiler"
msgstr ""

#: qml/ChatMessage.qml:190
#, kde-format
msgid "Download"
msgstr "Stáhnout"

#: qml/ConversationDisplay.qml:199
#, kde-format
msgid "Replying to multitarget messages is not supported"
msgstr "Odpověď na zprávy s vícero příjemci není podporováno"

#: qml/ConversationDisplay.qml:199
#, kde-format
msgid "Compose message"
msgstr "Napsat zprávu"

#: qml/ConversationList.qml:38 qml/ConversationList.qml:110
#, kde-format
msgid "No devices available"
msgstr "Žádná dostupná zařízení"

#: qml/ConversationList.qml:48
#, kde-format
msgid ""
"No new messages can be sent or received, but you can browse cached content"
msgstr ""
"Nebyly odeslány ani přijaty nové zprávy, ale můžete procházet uložený obsah"

#: qml/ConversationList.qml:54
#, kde-format
msgid "Refresh"
msgstr "Obnovit"

#: qml/ConversationList.qml:96
#, kde-format
msgid "Choose recipient"
msgstr "Zvolte příjemce"

#: qml/ConversationList.qml:101
#, kde-format
msgid "Cancel"
msgstr "Zrušit"

#: qml/ConversationList.qml:156
#, kde-format
msgid "Filter..."
msgstr "Filtr..."

#: qml/main.qml:50
#, kde-format
msgid "About..."
msgstr "O aplikaci..."
