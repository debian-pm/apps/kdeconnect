# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-08-18 02:35+0200\n"
"PO-Revision-Date: 2019-11-19 23:41+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.1\n"

#: kiokdeconnect.cpp:94
#, kde-format
msgid "Listing devices..."
msgstr "Seadmete tuvastamine..."

#: kiokdeconnect.cpp:139
#, kde-format
msgid "Accessing device..."
msgstr "Seadmega ühendumine..."

#: kiokdeconnect.cpp:155
#, kde-format
msgid "No such device: %0"
msgstr "Pole sellist seadet: %0"

#: kiokdeconnect.cpp:162
#, kde-format
msgid "%0 is not paired"
msgstr "%0 ei ole paardunud"

#: kiokdeconnect.cpp:167
#, kde-format
msgid "%0 is not connected"
msgstr "%0 ei ole ühendatud"

#: kiokdeconnect.cpp:172
#, kde-format
msgid "%0 has no Remote Filesystem plugin"
msgstr "%0 on ilma võrgufailisüsteemi pluginata"

#: kiokdeconnect.cpp:237
#, kde-format
msgid "Could not contact background service."
msgstr "Ühendumine taustateenusega nurjus."

#~ msgid "Could not mount device filesystem"
#~ msgstr "Seadme failisüsteemi ühendamine nurjus"
