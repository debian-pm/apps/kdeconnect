# Translation of kdeconnect-kcm to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-01-17 03:20+0100\n"
"PO-Revision-Date: 2019-02-09 11:15+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#: kcm.cpp:47
#, kde-format
msgid "KDE Connect Settings"
msgstr "Innstillingar for KDE Connect"

#: kcm.cpp:49
#, kde-format
msgid "KDE Connect Settings module"
msgstr "Modul for KDE Connect-innstillingar"

#: kcm.cpp:51
#, kde-format
msgid "(C) 2015 Albert Vaca Cintora"
msgstr "© 2015 Albert Vaca Cintora"

#: kcm.cpp:55
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kcm.cpp:262
#, kde-format
msgid "Available plugins"
msgstr "Tilgjengelege programtillegg"

#: kcm.cpp:315
#, kde-format
msgid "Error trying to pair: %1"
msgstr "Feil ved forsøk på paring: %1"

#: kcm.cpp:336
#, kde-format
msgid "(paired)"
msgstr "(para)"

#: kcm.cpp:339
#, kde-format
msgid "(not paired)"
msgstr "(ikkje para)"

#: kcm.cpp:342
#, kde-format
msgid "(incoming pair request)"
msgstr "(får paringsførespurnad)"

#: kcm.cpp:345
#, kde-format
msgid "(pairing requested)"
msgstr "(paringsførespurnad)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:47
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:70
#, kde-format
msgid "Edit"
msgstr "Rediger"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:92
#, kde-format
msgid "Save"
msgstr "Lagra"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:108
#, kde-format
msgid "Refresh"
msgstr "Oppdater"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:155
#, kde-format
msgid "Device"
msgstr "Eining"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:171
#, kde-format
msgid "(status)"
msgstr "(status)"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:210
#, kde-format
msgid "Accept"
msgstr "Godta"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:217
#, kde-format
msgid "Reject"
msgstr "Avvis"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:230
#, kde-format
msgid "Request pair"
msgstr "Be om paring"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:243
#, kde-format
msgid "Unpair"
msgstr "Løys paring"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:256
#, kde-format
msgid "Send ping"
msgstr "Send pingsignal"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:294
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://community.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>Inga eining valt.<br><br>Viss du har ei Android-"
"eining, installer <a href=\"https://play.google.com/store/apps/details?"
"id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: underline; color:"
"#4c6b8a;\">Android-appen KDE Connect</span></a> (òg tilgjengeleg <a href="
"\"https://f-droid.org/repository/browse/?fdid=org.kde.kdeconnect_tp\"><span "
"style=\" text-decoration: underline; color:#4c6b8a;\">frå F-Droid</span></"
"a>). Eininga skal då automatisk dukka opp her.<br><br>Får du det ikkje til å "
"fungera, sjå <a href=\"https://community.kde.org/KDEConnect\"><span style=\" "
"text-decoration: underline; color:#4c6b8a;\">wiki-sida for KDE Connect</"
"span></a> for hjelp.</p></body></html>"
