# Burkhard Lück <lueck@hube-lueck.de>, 2013, 2014, 2015, 2016, 2017, 2018, 2019.
# Frederik Schwarzer <schwarzer@kde.org>, 2015, 2016, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-01-17 03:20+0100\n"
"PO-Revision-Date: 2020-04-07 23:24+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Burkhard Lück"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lueck@hube-lueck.de"

#: kcm.cpp:47
#, kde-format
msgid "KDE Connect Settings"
msgstr "KDE-Connect-Einstellungen"

#: kcm.cpp:49
#, kde-format
msgid "KDE Connect Settings module"
msgstr "Einstellungsmodul für KDE Connect"

#: kcm.cpp:51
#, kde-format
msgid "(C) 2015 Albert Vaca Cintora"
msgstr "(C) 2015 Albert Vaca Cintora"

#: kcm.cpp:55
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kcm.cpp:262
#, kde-format
msgid "Available plugins"
msgstr "Verfügbare Module"

#: kcm.cpp:315
#, kde-format
msgid "Error trying to pair: %1"
msgstr "Fehler bei der Verbindung mit: %1"

#: kcm.cpp:336
#, kde-format
msgid "(paired)"
msgstr "(Verbunden)"

#: kcm.cpp:339
#, kde-format
msgid "(not paired)"
msgstr "(Nicht verbunden)"

#: kcm.cpp:342
#, kde-format
msgid "(incoming pair request)"
msgstr "(Eingehende Verbindungsanfrage)"

#: kcm.cpp:345
#, kde-format
msgid "(pairing requested)"
msgstr "(Kopplung angefragt)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:47
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:70
#, kde-format
msgid "Edit"
msgstr "Bearbeiten"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:92
#, kde-format
msgid "Save"
msgstr "Speichern"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:108
#, kde-format
msgid "Refresh"
msgstr "Aktualisieren"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:155
#, kde-format
msgid "Device"
msgstr "Gerät"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:171
#, kde-format
msgid "(status)"
msgstr "(Status)"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:210
#, kde-format
msgid "Accept"
msgstr "Annehmen"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:217
#, kde-format
msgid "Reject"
msgstr "Ablehnen"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:230
#, kde-format
msgid "Request pair"
msgstr "Verbindung anfragen"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:243
#, kde-format
msgid "Unpair"
msgstr "Verbindung trennen"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:256
#, kde-format
msgid "Send ping"
msgstr "Ping senden"

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:294
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://community.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>Kein Gerät ausgewählt.<br><br>Haben Sie ein Android-"
"Gerät, installieren Sie die Android-Anwendung <a href=\"https://play.google."
"com/store/apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-"
"decoration: underline; color:#4c6b8a;\">KDE Connect</span></a> (auch von<a "
"href=\"https://f-droid.org/repository/browse/?fdid=org.kde.kdeconnect_tp"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">F-Droid</"
"span></a> verfügbar) und sie sollte in der Liste erscheinen.<br><br>Bei "
"Problemen gehen Sie zur <a href=\"https://community.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"im Community Wiki</span></a>.</p></body></html>"

#~ msgid "(C) 2018 Nicolas Fella"
#~ msgstr "(C) 2018 Nicolas Fella"

#~ msgid "No device selected."
#~ msgstr "Kein Gerät ausgewählt."

#~ msgid ""
#~ "<html><head/><body><p>If you are having problems, visit the <a href="
#~ "\"https://community.kde.org/KDEConnect\"><span style=\" text-decoration: "
#~ "underline; \">KDE Connect Community wiki</span></a> for help.</p></body></"
#~ "html>"
#~ msgstr ""
#~ "<html><head/><body><p>Bei Problemen, besuchen Sie bitte das <a href="
#~ "\"https://community.kde.org/KDEConnect\"><span style=\" text-decoration: "
#~ "underline;\">Wiki der KDE-Connect-Gemeinschaft</span></a>.</p></body></"
#~ "html>"

#~ msgid "(trusted)"
#~ msgstr "(vertrauenswürdig)"

#~ msgid "Unavailable plugins"
#~ msgstr "Nich verfügbare Module"

#~ msgid "Plugins unsupported by the device"
#~ msgstr "Modul wird vom Gerät nicht unterstützt"

#~ msgid "Plugins"
#~ msgstr "Module"

#~ msgid "Browse"
#~ msgstr "Durchsehen"
