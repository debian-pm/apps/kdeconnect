# Burkhard Lück <lueck@hube-lueck.de>, 2015, 2017, 2018, 2019.
# Frederik Schwarzer <schwarzer@kde.org>, 2016, 2018, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-03-16 09:04+0100\n"
"PO-Revision-Date: 2020-04-07 23:27+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.12.3\n"

#: package/contents/ui/Battery.qml:40
msgid "%1% charging"
msgstr "%1% wird geladen"

#: package/contents/ui/Battery.qml:40
msgid "%1%"
msgstr "%1 %"

#: package/contents/ui/Battery.qml:40
msgid "No info"
msgstr "Keine Information"

#: package/contents/ui/DeviceDelegate.qml:65
msgid "File Transfer"
msgstr "Dateiübertragung"

#: package/contents/ui/DeviceDelegate.qml:66
msgid "Drop a file to transfer it onto your phone."
msgstr "Legen Sie eine Datei ab, um sie auf Ihr Telefon zu übertragen"

#: package/contents/ui/DeviceDelegate.qml:84
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/DeviceDelegate.qml:105
msgid "Please choose a file"
msgstr "Bitte wählen Sie eine Datei"

#: package/contents/ui/DeviceDelegate.qml:113
msgid "Share file"
msgstr "Datei weitergeben"

#: package/contents/ui/DeviceDelegate.qml:133
msgid "Ring my phone"
msgstr "Mein Telefon anklingeln"

#: package/contents/ui/DeviceDelegate.qml:151
msgid "Browse this device"
msgstr "Dieses Gerät durchsuchen"

#: package/contents/ui/DeviceDelegate.qml:168
msgid "SMS Messages"
msgstr "SMS-Nachrichten"

#: package/contents/ui/DeviceDelegate.qml:189
msgid "Remote Keyboard"
msgstr "Entfernte Tastatur"

#: package/contents/ui/DeviceDelegate.qml:205
msgid "Notifications:"
msgstr "Benachrichtigungen:"

#: package/contents/ui/DeviceDelegate.qml:212
msgid "Dismiss all notifications"
msgstr "Alle Benachrichtigungen abweisen"

#: package/contents/ui/DeviceDelegate.qml:248
msgid "Reply"
msgstr "Antworten"

#: package/contents/ui/DeviceDelegate.qml:257
msgid "Dismiss"
msgstr "Abweisen"

#: package/contents/ui/DeviceDelegate.qml:274
msgid "Run command"
msgstr "Befehl ausführen"

#: package/contents/ui/DeviceDelegate.qml:282
msgid "Add command"
msgstr "Befehl hinzufügen"

#: package/contents/ui/FullRepresentation.qml:56
#, fuzzy
#| msgid "No paired devices available"
msgid "No paired devices"
msgstr "Es sind keine angeschlossenen Geräte verfügbar"

#: package/contents/ui/FullRepresentation.qml:56
#, fuzzy
#| msgid "No paired devices available"
msgid "Paired device is unavailable"
msgid_plural "All paired devices are unavailable"
msgstr[0] "Es sind keine angeschlossenen Geräte verfügbar"
msgstr[1] "Es sind keine angeschlossenen Geräte verfügbar"

#: package/contents/ui/FullRepresentation.qml:65
msgid "Install KDE Connect on your Android device to integrate it with Plasma!"
msgstr ""
"Installieren Sie KDE Connect auf Ihren Android-Gerät, um es in Plasma zu "
"integrieren."

#: package/contents/ui/FullRepresentation.qml:76
msgid "Install from Google Play"
msgstr "Von Google Play installieren"

#: package/contents/ui/FullRepresentation.qml:86
msgid "Install from F-Droid"
msgstr "Von F-Droid installieren"

#: package/contents/ui/FullRepresentation.qml:92
msgid "Pair a Device..."
msgstr ""

#: package/contents/ui/FullRepresentation.qml:92
msgid "Configure..."
msgstr ""

#: package/contents/ui/main.qml:66
msgid "KDE Connect Settings..."
msgstr "KDE-Connect-Einstellungen ..."

#~ msgid "Share text"
#~ msgstr "Text weitergeben"

#~ msgid "Search for devices"
#~ msgstr "Nach Geräten suchen"

#~ msgid "Charging: %1%"
#~ msgstr "Wird geladen: %1 %"

#~ msgid "Discharging: %1%"
#~ msgstr "Wird entladen: %1 %"

#~ msgid "Battery"
#~ msgstr "Akku"
