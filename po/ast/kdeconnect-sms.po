# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# enolp <enolp@softastur.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-02-04 03:16+0100\n"
"PO-Revision-Date: 2020-02-25 18:56+0100\n"
"Last-Translator: enolp <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Softastur"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alministradores@softastur.org"

#: conversationlistmodel.cpp:185 conversationmodel.cpp:135
#, kde-format
msgid "(Unsupported Message Type)"
msgstr "(Nun se sofita la triba del mensaxe)"

#: conversationlistmodel.cpp:189
#, kde-format
msgid "You: %1"
msgstr "Tu: %1"

#: conversationlistmodel.cpp:195
#, kde-format
msgid "%1: %2"
msgstr "%1: %2"

#: main.cpp:39
#, kde-format
msgid "SMS Instant Messaging"
msgstr ""

#: main.cpp:41 qml/main.qml:35
#, kde-format
msgid "KDE Connect SMS"
msgstr "SMS de KDE Connect"

#: main.cpp:43
#, kde-format
msgid "(C) 2018-2019, KDE Connect Team"
msgstr "(C) 2018-2019, L'equipu de KDE Connect"

#: main.cpp:44
#, kde-format
msgid "Simon Redman"
msgstr "Simon Redman"

#: main.cpp:45
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: main.cpp:46
#, kde-format
msgid "Nicolas Fella"
msgstr "Nicolas Fella"

#: main.cpp:54
#, kde-format
msgid "Select a device"
msgstr ""

#: main.cpp:54
#, kde-format
msgid "id"
msgstr ""

#: main.cpp:55
#, kde-format
msgid "Send a message"
msgstr ""

#: main.cpp:55
#, kde-format
msgid "message"
msgstr ""

#: qml/ChatMessage.qml:122
#, kde-format
msgid "Copy Message"
msgstr "Copiar el mensaxe"

#: qml/ChatMessage.qml:150
#, kde-format
msgid "Spoiler"
msgstr ""

#: qml/ChatMessage.qml:190
#, kde-format
msgid "Download"
msgstr ""

#: qml/ConversationDisplay.qml:199
#, kde-format
msgid "Replying to multitarget messages is not supported"
msgstr "Nun se sofita la rempuesta a munchos contautos"

#: qml/ConversationDisplay.qml:199
#, kde-format
msgid "Compose message"
msgstr ""

#: qml/ConversationList.qml:38 qml/ConversationList.qml:110
#, kde-format
msgid "No devices available"
msgstr "Nun hai preseos disponibles"

#: qml/ConversationList.qml:48
#, kde-format
msgid ""
"No new messages can be sent or received, but you can browse cached content"
msgstr ""
"Nun hai mensaxes nuevos que puean unviase o recibise pero pues restolar el "
"conteníu de la caché"

#: qml/ConversationList.qml:54
#, kde-format
msgid "Refresh"
msgstr "Refrescar"

#: qml/ConversationList.qml:96
#, kde-format
msgid "Choose recipient"
msgstr ""

#: qml/ConversationList.qml:101
#, kde-format
msgid "Cancel"
msgstr "Encaboxar"

#: qml/ConversationList.qml:156
#, kde-format
msgid "Filter..."
msgstr "Peñerar…"

#: qml/main.qml:50
#, kde-format
msgid "About..."
msgstr "Tocante a…"
